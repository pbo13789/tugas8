public class Rectangle extends Shape {
    double width = 1.0;
    double length = 1.0;

    public Rectangle(){
        System.out.println("yaitu objek bangun datar rectangle");
    }

    public Rectangle(double width, double length){
        this.width = width;
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea(){
        return getArea();
    }

    public double getPerimeter(){
        return getPerimeter();
    }

    @Override
    public String toString() {
        System.out.println("Width " + getWidth());
        System.out.println("Length " + getLength());
        return super.toString();
    }
}
