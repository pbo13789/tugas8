public class Main {
    public static void main(String[] args) {
        Shape shape = new Shape();
        shape.toString();
        System.out.print("\nAda pengisian Class shape2 secara langsung menggunakan parameter\n");
        Shape shape2 = new Shape("Hijau", false);
        shape2.toString();

        System.out.println("\nLalu Bangun Datar Cirle");
        Circle circle = new Circle();
        circle.toString();
        Circle circle2 = new Circle(5.0);
        System.out.println("Disini saya mengubah radiusnya");
        circle2.toString();
        Circle circle3 = new Circle(5.4, false);
        System.out.println("Disini saya mengubah radiusnya serta fieldnya");
        circle3.toString();

        System.out.println("\nLalu bangun datar Rectangle");
        Rectangle rectangle = new Rectangle();
        rectangle.toString();
        Rectangle rectangle2 = new Rectangle(3.8, 2.8);
        System.out.println("Disini saya mengubah width, length, dan field");
        rectangle2.setField(false); 
        rectangle2.toString();

        System.out.println("\nLalu bangun datar Square");
        Square square = new Square();
        square.toString();
        Square square2 = new Square(7.7);
        square2.setColor("Jingga");
        square2.setWidth(5.7);
        square2.setLength(4.6);
        square2.setField(false);
        System.out.println("Merubah nilai side, width, length, field, color");
        square2.toString();
        System.out.println("Merubah nilai side, color, field dengan parameter");
        Square square3 = new Square(3.6, "Biru", false);
        square3.toString();
    }
}
