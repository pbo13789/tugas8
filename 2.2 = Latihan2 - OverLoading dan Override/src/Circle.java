public class Circle extends Shape{
    double radius = 1.0;
    public Circle(){
        System.out.print("yaitu Object Bangun Datar circle... \n");
    }
    
    public Circle(double radius){
        this.radius = radius;
    }

    public Circle(double radius, boolean field){
        this.radius = radius;
        super.field = field;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea(){
        System.out.println("Radius " + getRadius());
        return getArea();
    }

    public double getPerimeter(){
        return getPerimeter();
    }

    @Override
    public String toString() {
        System.out.println("Radius " + getRadius());
        return super.toString();
    }
}
