public class Shape {
    String color = "red";
    boolean field = true;

    public Shape(){
        System.out.print("Object Shape dibentuk...");
    }

    public Shape(String color, boolean field){
        this.color = color;
        this.field = field;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isField() {
        return this.field;
    }

    public void setField(boolean field) {
        this.field = field;
    }

    @Override
    public String toString() {
        System.out.println("Warna " + getColor());
        System.out.println("Field " + isField());
        return super.toString();
    }
}
