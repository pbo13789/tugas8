public class Square extends Rectangle {
    double side;

    public Square(){
        System.out.println("yang Berbentuk Square");
    }

    public Square(double side){
        System.out.println("yang Berbentuk Square");
        this.side = side;
    }

    public Square(double side, String color, boolean field){
        System.out.println("yang Berbentuk Square");
        this.side = side;
        this.color = color;
        this.field = field;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    @Override
    public void setWidth(double width) {
        super.setWidth(width);
        this.width = width;
    }

    @Override
    public void setLength(double length) {
        super.setLength(length);
        this.length = length;
    }

    @Override
    public String toString() {
        System.out.println("Side " + this.side);
        System.out.println("Width " + this.width);
        System.out.println("length " + this.length);
        return super.toString();
    }
}
