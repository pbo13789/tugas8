import java.util.Scanner;

public class TesPesawat {
    public static void main(String[] args) {
        Scanner inp = new Scanner(System.in);
        Pesawat psw = new Pesawat();
        PesawarTempur pswT = new PesawarTempur();

        psw.sayap = inp.next();
        psw.terbang();
        psw.mendarat();
        pswT.ekor = inp.next();
        pswT.terbang();
        pswT.mendarat();
        pswT.manuver();

        inp.close();
    }
}
