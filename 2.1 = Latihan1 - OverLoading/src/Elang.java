public class Elang extends Hewan{

    public Elang(){
        jenis = "Burung Predator";
        System.out.println("Jenis Hewan " + jenis);
        ciri = "Hidup mandiri membunuh ular";
        System.out.println("Ciri hewan ini " + ciri);
    }

    @Override
    public void berjalan() {
        super.berjalan();
        System.out.println("Mengepakan Sayap");
    }

    @Override
    public void suara() {
        super.suara();
        System.out.println("Ciat ciattt");
    }

    public void bernafas(){
        System.out.println("Hewan ini bernafas menggunakan Paru-Paru");
    }
}
