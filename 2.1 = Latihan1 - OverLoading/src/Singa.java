public class Singa extends Hewan {

    public Singa(){
        jenis = "Mamalia";
        System.out.println("Jenis Hewan " + jenis);
        ciri = "Hidup Berkeleompok";
        System.out.println("Ciri hewan ini " + ciri);
    }

    @Override
    public void suara() {
        super.suara();
        System.out.println("Waaaaunggg...");
    }

    @Override
    public void berjalan() {
        super.berjalan();
        
        System.out.println("Kaki yang kuat");
    }

    public void bernafas(){
        System.out.println("Hewan ini bernafas menggunakan Paru-Paru");
    }
}
