public class Lebah extends Hewan{

    public Lebah(){
        jenis = "Serangga";
        System.out.println("Jenis Hewan " + jenis);
        ciri = "Menghasilkan madu";
        System.out.println("Ciri hewan ini " + ciri);
    }

    @Override
    public void berjalan() {
        super.berjalan();
        System.out.println("Mengepakkan sayap untuk mencari makanan");
    }
    @Override
    public void suara() {
        super.suara();
        System.out.println("NGGGNGGGGG");
    }

    public void bernafas(){
        System.out.println("Hewan ini bernafas menggunakan trakea");
    }

}
