public class Paus extends Hewan{

    public Paus(){
        jenis = "Mamalia";
        System.out.println("Jenis Hewan " + jenis);
        ciri = "ada beberapa, misal paus orca dia berkelompok dan pemangsa";
        System.out.println("Ciri hewan ini " + ciri);
    }

    @Override
    public void berjalan() {
        super.berjalan();
        System.out.println("Berenang dengan ekor");
    }
    @Override
    public void suara() {
        super.suara();
        System.out.println("Suaranya seperti gelombang untuk mendeteksi mangsa");
        
    }

    public void bernafas(){
        System.out.println("Hewan ini bernafas menggunakan Paru-Paru");
    }

}
