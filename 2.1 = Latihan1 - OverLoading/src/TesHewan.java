import java.util.Scanner;

public class TesHewan {
    public static void main(String[] args) {
        Scanner inp = new Scanner(System.in);
        String Option;
        Boolean YesNo= true;

        while(YesNo){
            System.out.println("\nDatabase");
            System.out.println("1. \tSinga");
            System.out.println("2. \tElang");
            System.out.println("3. \tLebah");
            System.out.println("4. \tPaus");
            System.out.println("5. \tKeluar");
            System.out.print("Pilihan: ");
            Option = inp.next();

            switch(Option){
                case "1":
                    System.out.println("Singa");
                    Singa s = new Singa();
                    s.berjalan();
                    s.suara();
                    s.bernafas();
                    break;
                case "2":
                    System.out.println("Elang");
                    Elang e = new Elang();
                    e.berjalan();
                    e.suara();
                    e.bernafas();
                    break;
                case "3":
                    System.out.println("Lebah");
                    Lebah l = new Lebah();
                    l.berjalan();
                    l.suara();
                    l.bernafas();
                    break;
                case "4":
                    System.out.println("Paus");
                    Paus p = new Paus();
                    p.berjalan();
                    p.suara();
                    p.bernafas();
                    break;
                case "5":
                    YesNo = false;
                    break;
                default:
                    System.out.println("Option tidak ditemukan [1-5]");

            }
            
        }
        inp.close();
    }
}
