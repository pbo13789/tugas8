public class MatematikaDemo {
    public static void main(String[] args) {
        Matematika mtk = new Matematika();

        System.out.println("pengurangan int " + mtk.pengurangan(12, 4));
        System.out.println("penambahan int " + mtk.penambahan(12, 3));
        System.out.println("perkalian int " + mtk.perkalian(3, 3));
        System.out.println("pembagian int " + mtk.pembagian(12, 2));

        System.out.println("pengurangan double " + mtk.pengurangan(15, 3.5, 2.8));
        System.out.println("penambahan double " + mtk.penambahan(54, 2, 5));
        System.out.println("perkalian double " + mtk.perkalian(4, 2, 6));
        System.out.println("pembagian double " + mtk.pembagian(20, 5, 2));
    }
}
