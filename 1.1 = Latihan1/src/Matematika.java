public class Matematika {

    public int pengurangan(int num1, int num2){
        return num1 - num2;
    }

    public int penambahan(int num1, int num2){
        return num1 + num2;
    }

    public int perkalian(int num1, int num2){
        return num1 * num2;
    }

    public int pembagian(int num1, int num2){
        return num1 / num2;
    }

    public double pengurangan(double num1, double num2, double num3){
        return num1-num2-num3;
    }

    public double penambahan(double num1, double num2, double num3){
        return num1+num2+num3;
    }

    public double perkalian(double num1, double num2, double num3){
        return (num1*num2)*num3;
    }

    public double pembagian(double num1, double num2, double num3){
        return (num1/num2)/num3;
    }
}
